package asst.thatsbiz;

/**
 * @author Material Gain
 * @since 2019 06
 */
public class PrintTagsTestMain {

  /** exercise the program which prints intervals between time tags
   * @param args ignored
   */
  public static void main(String[] args) {
    for (int i=0; i<Stomp.TIMES.length; i++) {
      Stomp.TIMES[i] = 1000*i;
    }
    System.out.println(Stomp.printTagIntervals(Stomp.TIMETAGS, Stomp.TIMES));
  }
}
