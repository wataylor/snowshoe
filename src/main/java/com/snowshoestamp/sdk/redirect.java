package com.snowshoestamp.sdk;

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import asst.thatsbiz.Sneakret;

/**
 * @author showshoe.com
 *
 */
public class redirect extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse res)
    throws IOException, ServletException
    {
    	 res.setContentType("text/plain");
         String site="http://beta.snowshoestamp.com/api/v2/client/"
    	 + Sneakret.app_key +"/";
         res.setStatus(res.SC_MOVED_TEMPORARILY);
         res.setHeader("Location", site);

    }

}
