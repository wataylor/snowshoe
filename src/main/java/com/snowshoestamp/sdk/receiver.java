package com.snowshoestamp.sdk;

import java.io.*;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.commonshttp.*;

import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import asst.thatsbiz.Sneakret;




public class receiver extends HttpServlet {
	
    public void doPost(HttpServletRequest request, HttpServletResponse res)
    throws IOException, ServletException
    {
   	
    	OAuthConsumer consumer = new CommonsHttpOAuthConsumer(
                Sneakret.app_key,
                Sneakret.app_secret);
    	
    	res.setContentType("text/html");
        PrintWriter out = res.getWriter();
    	InputStream input = request.getInputStream();
    	BufferedReader in = new BufferedReader(new InputStreamReader(input));
    	String inputLine;
    	inputLine = in.readLine();
    	String body=inputLine;
    	body=body.substring(5);
    	body = URLDecoder.decode(body, "UTF-8");

    	
    	ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
    	params.add(new BasicNameValuePair("data", body));
    	
    	HttpPost connection = new HttpPost("http://beta.snowshoestamp.com/api/v2/stamp");
    	connection.setEntity(new UrlEncodedFormEntity(params));
    	connection.setHeader("Content-Type", "application/x-www-form-urlencoded");
  
    	
    	try {
			consumer.sign(connection);;
		} catch (OAuthMessageSignerException e) {
			e.printStackTrace();
		} catch (OAuthExpectationFailedException e) {
			e.printStackTrace();
		} catch (OAuthCommunicationException e) {
			e.printStackTrace();
		}
    	
    	consumer.setTokenWithSecret("", "");
    	HttpClient httpClient = new DefaultHttpClient();
        HttpResponse response = httpClient.execute(connection);
        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
        String json = reader.readLine();
        out.println(json);// This will print JSON response to the screen, but you can do whatever you want with it
      }
    }






