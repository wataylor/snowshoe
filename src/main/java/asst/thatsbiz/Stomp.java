package asst.thatsbiz;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

/** Receive data from a snowshoe stamp and convert it to a stamp ID
 * This is a proof of concept to handle odd cases, not production code.
 * @author Material Gain
 * @since 2019 06
 */
public class Stomp extends HttpServlet {

  /** Keep the compiler happy */
  private static final long serialVersionUID = 1L;
  /** Labels for time tags*/
  public static final String[] TIMETAGS = {
    "Start", "Printed", "Made Auth", "New Conn.", "Signed",
    "Requested", "Done"
  };
  /** The next TIMES array element to be used.*/
  public static int TIMETAG = 0;
  /** Array of system times to be matched with TIMETAGS array.*/
  public static long[] TIMES = new long[TIMETAGS.length];

  /** Generate a message giving intervals between times as well as the
   * overall elapsed time
   * @param tags labels for the times
   * @param times array of millisecond time tags
   * @return labeled intervals between time tags and an overall time
   */
  public static StringBuilder printTagIntervals(String[] tags, long[] times) {
    StringBuilder sb = new StringBuilder();
    int len = times.length;
    for (int i = 1; i<len; i++) {
      sb.append(tags[i-1] + " to " + tags[i] + " \t" + (times[i] - times[i-1]) + "\n");
    }
    sb.append(tags[0] + " to " + tags[len-1] + "\t" + (times[len-1] - times[0]) + "\n");
    return sb;
  }

  /**
   * Convert an input stream to a string by reading everything in the stream and
   * passing it back.
   *
   * @param stream
   *          input stream created by opening the URL
   * @return contents of the stream as a string
   */
  public static String streamToString(InputStream stream) {
    StringBuilder sb = new StringBuilder();
    byte[] buffer = new byte[2000]; // Read 2KB at a whack
    int howMany; // How many bytes were read at a time.

    try {
      while ((howMany = stream.read(buffer, 0, 2000)) > 0) {
	/*
	 * Convert the buffer to a string using the default character encoding
	 * for this locale. This works ONLY if the input stream was generated
	 * using the same locale as the default locale where the program is run.
	 * It would be better to specify the locale which matches the source of
	 * the stream, but this would require considerably more research.
	 */
	sb.append(new String(buffer, 0, howMany));
      }
    } catch (Exception e) {
      System.out.println(e.getMessage());
      throw new RuntimeException(e);
    }
    return sb.toString();
  }

  @Override
  public void service (HttpServletRequest request, HttpServletResponse jsResp)
      throws IOException, ServletException {
    TIMES[TIMETAG++] = System.currentTimeMillis();
    jsResp.setContentType("text/html");

    PrintWriter out = jsResp.getWriter();
    out.println("<html><head><title>Snows</title></head><body>");
    String msg = "Event key was " + request.getParameter("ev");
    System.out.println(msg);
    out.println(msg + "<br>");
    String stamp;
    msg = "Stamp was " + (stamp = request.getParameter("stamp"));
    System.out.println(msg);
    out.println(msg + "<br>");
    msg = "Length was " + request.getParameter("len");
    System.out.println(msg);
    out.println(msg + "<br>");
    msg = "Numbers was " + request.getParameter("numbers");
    System.out.println(msg);
    out.println(msg + "<br>");

    TIMES[TIMETAG++] = System.currentTimeMillis();
    OAuthConsumer consumer = new CommonsHttpOAuthConsumer(Sneakret.app_key, Sneakret.app_secret);

    ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
    params.add(new BasicNameValuePair("data", stamp));
    TIMES[TIMETAG++] = System.currentTimeMillis();

    HttpPost connection = new HttpPost("https://beta.snowshoestamp.com/api/v2/stamp");
    connection.setEntity(new UrlEncodedFormEntity(params));
    connection.setHeader("Content-Type", "application/x-www-form-urlencoded");
    msg = "requestLine " + connection.getRequestLine();
    System.out.println(msg);
    out.println(msg + "<br>");
    msg = "Path " + connection.getURI().getPath();
    System.out.println(msg);
    out.println(msg + "<br>");
    TIMES[TIMETAG++] = System.currentTimeMillis();

    try {
      consumer.sign(connection);
    } catch (OAuthMessageSignerException e) {
      e.printStackTrace();
    } catch (OAuthExpectationFailedException e) {
      e.printStackTrace();
    } catch (OAuthCommunicationException e) {
      e.printStackTrace();
    }
    consumer.setTokenWithSecret("", "");
    TIMES[TIMETAG++] = System.currentTimeMillis();

    Header[] headers = connection.getAllHeaders();
    if ((headers != null) && (headers.length > 0)) {
      System.out.println("\nHTTP Post Headers");
      for (Header h : headers) {
	System.out.println(h.getName() + " " + h.getValue());
      }
    }

    msg = "Signed";
    System.out.println(msg);
    out.println(msg + "<br>");

    String json = "";
    CloseableHttpClient httpClient = HttpClients.createDefault();
    CloseableHttpResponse response = httpClient.execute(connection);
    StatusLine statusLine = response.getStatusLine();
    msg = statusLine.getStatusCode() + " " + statusLine.getReasonPhrase();
    System.out.println(msg);
    out.println(msg + "<br>");
    TIMES[TIMETAG++] = System.currentTimeMillis();

    try {
      HttpEntity entity = response.getEntity();
      if (entity != null) {
	BufferedReader reader = new BufferedReader(new InputStreamReader(entity
	    .getContent(), "UTF-8"));
	json = reader.readLine();
	reader.close();
      }
    } finally {
      response.close();
    }    
    TIMES[TIMETAG++] = System.currentTimeMillis();
    System.out.println(json);
    out.println(json);
    out.println("</body>");
    System.out.println(printTagIntervals(TIMETAGS, TIMES));
  }
}
