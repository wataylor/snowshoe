<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ShowF</title>
<script type="text/javascript" language="JavaScript">
	var testEvent3 = JSON
			.parse('{"touches":[{"pageX": 1, "pageY":1},{"pageX":100, "pageY":100},{"pageX": 200, "pageY":200}]}');
	var testEvent5 = JSON
			.parse('{"touches":[{"pageX": 1, "pageY":1},{"pageX":100, "pageY":100},{"pageX": 200, "pageY":200},{"pageX":300, "pageY":300}, {"pageX":400, "pageY":400}]}');
	//
	// Base64 adapted from & courtesy of http://scotch.io
	//
	var _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
	function encode(input) {
		var output = "";
		var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		var i = 0;
		input = utf8_encode(input);
		while (i < input.length) {
			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);
			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;
			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}
			output = output + this._keyStr.charAt(enc1)
					+ this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3)
					+ this._keyStr.charAt(enc4);
		}
		return output;
	};
	function whinge(message) {
		document.getElementById("whinge").innerHTML = message;
		setTimeout(function() {
			document.getElementById("whinge").innerHTML = ""
		}, 3000);
	};
	function utf8_encode(string) {
		//string = string.replace(/\r\n/g,"\n");
		var utftext = "";
		for (var n = 0; n < string.length; n++) {
			var c = string.charCodeAt(n);
			if (c < 128) {
				utftext += String.fromCharCode(c);
			} else if ((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			} else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
		}
		return utftext;
	};
	function gotMe(event) {
		var data = [];
		var touches = event.touches;
		if (event.touches.length <= 3) {
			return;
		}
		var lert = "";
		for (var i = 0; i <= event.touches.length; i++) {
			if (touches[i]) {
				data.push([ touches[i].pageX, touches[i].pageY ]);
				lert = lert + " " + touches[i].pageX + "," + touches[i].pageY;
			}
		}
		if (event.touches.length < 5) {
			document.getElementById("whinge").innerHTML = "The stamp has 5 dots, but we read "
					+ event.touches.length
					+ ".  Wiggle the stamp a bit and hold for 3 seconds.";
			setTimeout(function() {
				document.getElementById("whinge").innerHTML = ""
			}, 5000);
			return;
		}
		var formElem = document.getElementById("formStamp");
		formElem.value = encode(JSON.stringify(data));
		formElem = document.getElementById("len");
		formElem.value = event.touches.length;
		formElem = document.getElementById("numbers");
		formElem.value = lert;
		whinge("Sending it!  Thanks.");
		document.subForm.submit();
	};
</script>
</head>
<body id="stampscreen">
	<h1>
		Stamp the screen in the box. This text<br>is meant to push the
		stamp<br> sensitive area down<br> to provide a body that<br>can
		be scrolled<br> to move the stamp-sensitive area.
	</h1>
	<button type="button"
		style="height: 45px; width: 100%; font-size: 110%; background-color: #ABFCA9"
		onClick="whinge('above')">Button above the stamp area to
		generate a message</button>
	<div
		style="height: 825px; width: 925px; border-style: solid; border-width: 5px">
		<h1 id="whinge"></h1>
		<h1>Stamp the screen</h1>
		<h1>Stamp the screen</h1>
		<h1>Stamp the screen</h1>
		<button type="button"
			style="height: 45px; width: 100%; font-size: 110%; background-color: #ABFCA9"
			onClick="whinge('middle')">Button in the middle of the stamp
			area to generate a message</button>
		<h1>Stamp the screen</h1>
	</div>
	<button type="button"
		style="height: 45px; width: 100%; font-size: 110%; background-color: #ABFCA9"
		onClick="whinge('below')">Button below the stamp area to
		generate a message</button>
	<form name="subForm" id="subForm" action="stomp" accept-charset="utf-8">
		<input type="hidden" id="formStamp" name="stamp" value=""> <input
			type="hidden" name="ev" value="ourkey"> <input type="hidden"
			id="len" name="len" value=""> <input type="hidden"
			id="numbers" name="numbers" value="">
	</form>
	<button type="button"
		style="height: 45px; width: 100%; font-size: 110%; background-color: #ABFCA9"
		onClick="gotMe(testEvent3)">Test with exactly 3 points</button>
	<button type="button"
		style="height: 45px; width: 100%; font-size: 110%; background-color: #ABFCA9"
		onClick="gotMe(testEvent5)">Test with exactly 5 points</button>
	<h1>
		Stamp the screen in the box above. This text<br>is meant to give
		room below the stamp-<br> sensitive area<br> to provide a
		body that<br>can be scrolled past<br> the stamp-sensitive
		area.<br> The screen can be expanded and contracted<br>to
		see how the stamp system acts.<br> Changing with scale with 2
		fingers does not trigger the stamp<br> message. Changing with 3
		fingers does.
	</h1>
	<script type="text/javascript" language="JavaScript">
		var elem = document.getElementById("stampscreen");
		elem.addEventListener('touchstart', function(event) {
			gotMe(event);
		});
	</script>
</body>
</html>
