<html>
<% String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<body>
<h2>Hello World!</h2>
<%= basePath %>
<table cellspacing="2" cellpadding="2" border="2">
<tr><td>AuthType</td><td><%= request.getAuthType()%></td></tr>
<tr><td>CharacterEncoding</td><td><%= request.getCharacterEncoding()%></td></tr>
<tr><td>ContextPath</td><td><%= request.getContextPath()%></td></tr>
<tr><td>LocalAddr</td><td><%= request.getLocalAddr()%></td></tr>
<tr><td>LocalName</td><td><%= request.getLocalName()%></td></tr>
<tr><td>Locale</td><td><%= request.getLocale()%></td></tr>
<tr><td>PathInfo</td><td><%= request.getPathInfo()%></td></tr>
<tr><td>QueryString</td><td><%= request.getQueryString()%></td></tr>
<tr><td>RemoteAddr</td><td><%= request.getRemoteAddr()%></td></tr>
<tr><td>RemoteHost</td><td><%= request.getRemoteHost()%></td></tr>
<tr><td>RequestURI</td><td><%= request.getRequestURI()%></td></tr>
<tr><td>Scheme</td><td><%= request.getScheme()%></td></tr>
<tr><td>ServerName</td><td><%= request.getServerName()%></td></tr>
<tr><td>ServletPath</td><td><%= request.getServletPath()%></td></tr>
</table></body>
</html>
